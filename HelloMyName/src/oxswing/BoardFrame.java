/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxswing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.Action;
import javax.swing.JButton;

/**
 *
 * @author Gun
 */
public class BoardFrame extends javax.swing.JFrame {

    private int row;
    private int col;
    JButton btnTables[][] = null;
    Table table;
    Player o;
    Player x;

    /**
     * Creates new form BoradFrame
     */
    public BoardFrame() {
        initComponents();
        initTable();
        initGame();
        showTable();
        showTurn();
        hideNewGame();
        btnNewGame(
        );
    }

    public void btnNewGame() {
        btnNewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                newGame();
            }
        }
        );
    }

    private void initGame() {
        PlayerService.load();
        o = PlayerService.getO();
        x = PlayerService.getX();
        table = new Table(o, x);
    }

    private void initTable() {
        JButton tables[][] = {{btnTable1, btnTable2, btnTable3},
        {btnTable4, btnTable5, btnTable6},
        {btnTable7, btnTable8, btnTable9}};
        btnTables = tables;
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strCom = e.getActionCommand();
                        String coms[] = strCom.split(",");
                        row = Integer.parseInt(coms[0]);
                        col = Integer.parseInt(coms[1]);
//                        showRowCol();
                        if (table.setRowCol(row, col)) {
                            showTable();
                            if (table.checkWin()) {
                                showWin();
                                return;
                            }
                            switchTurn();

                        }

                    }

                });
            }

        }
    }

    private void showRowCol() {
        lblRowCol.setText("Row: " + row + ",Col:" + col);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnTable1 = new javax.swing.JButton();
        btnTable2 = new javax.swing.JButton();
        btnTable3 = new javax.swing.JButton();
        btnTable4 = new javax.swing.JButton();
        btnTable5 = new javax.swing.JButton();
        btnTable6 = new javax.swing.JButton();
        btnTable7 = new javax.swing.JButton();
        btnTable8 = new javax.swing.JButton();
        btnTable9 = new javax.swing.JButton();
        lblRowCol = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        ODraw = new javax.swing.JLabel();
        OWin = new javax.swing.JLabel();
        OLose = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        XDraw = new javax.swing.JLabel();
        XWin = new javax.swing.JLabel();
        XLose = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTable1.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable1.setText("-");
        btnTable1.setActionCommand("1,1");
        btnTable1.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable1.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable1.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable2.setText("-");
        btnTable2.setActionCommand("1,2");
        btnTable2.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable2.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable2.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable3.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable3.setText("-");
        btnTable3.setActionCommand("1,3");
        btnTable3.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable3.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable3.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable4.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable4.setText("-");
        btnTable4.setActionCommand("2,1");
        btnTable4.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable4.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable4.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable5.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable5.setText("-");
        btnTable5.setActionCommand("2,2");
        btnTable5.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable5.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable5.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable6.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable6.setText("-");
        btnTable6.setActionCommand("2,3");
        btnTable6.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable6.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable6.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable7.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable7.setText("-");
        btnTable7.setActionCommand("3,1");
        btnTable7.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable7.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable7.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable8.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable8.setText("-");
        btnTable8.setActionCommand("3,2");
        btnTable8.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable8.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable8.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable9.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        btnTable9.setText("-");
        btnTable9.setActionCommand("3,3");
        btnTable9.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable9.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable9.setPreferredSize(new java.awt.Dimension(80, 80));

        lblRowCol.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblRowCol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRowCol.setText("Row: ?, Col: ?");

        btnNewGame.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnNewGame.setText("New Game");

        jPanel4.setBackground(new java.awt.Color(255, 255, 0));

        ODraw.setText("ODraw : ");

        OWin.setText("OWin : ");

        OLose.setText("OLose : ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ODraw, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(OLose, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(OWin, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 21, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(OWin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OLose, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ODraw, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(0, 255, 255));

        XDraw.setText("XDraw : ");

        XWin.setText("XWin : ");

        XLose.setText("XLose : ");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(XLose, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(30, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(XWin, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(XDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(XWin, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(XLose, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(XDraw, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        btnSave.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnNewGame, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblRowCol, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblRowCol, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNewGame, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                    .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        PlayerService.save();
    }//GEN-LAST:event_btnSaveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BoardFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoardFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ODraw;
    private javax.swing.JLabel OLose;
    private javax.swing.JLabel OWin;
    private javax.swing.JLabel XDraw;
    private javax.swing.JLabel XLose;
    private javax.swing.JLabel XWin;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnTable1;
    private javax.swing.JButton btnTable2;
    private javax.swing.JButton btnTable3;
    private javax.swing.JButton btnTable4;
    private javax.swing.JButton btnTable5;
    private javax.swing.JButton btnTable6;
    private javax.swing.JButton btnTable7;
    private javax.swing.JButton btnTable8;
    private javax.swing.JButton btnTable9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblRowCol;
    // End of variables declaration//GEN-END:variables
    private void hideNewGame() {
        btnNewGame.setVisible(false);
    }

    private void showNewGame() {
        btnNewGame.setVisible(true);
        showScore();
    }

    private void showTable() {
        char data[][] = table.getData();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setText("" + data[i][j]);
            }
        }
    }

    private void switchTurn() {
        table.switchPlayer();
        showTurn();
    }

    public void showTurn() {
        lblRowCol.setText("Turn :" + table.getCurrentPlayer().getName());
    }

    public void showWin() {
        if (table.getWinner() == null) {
            lblRowCol.setText("Drawww !!!");
        } else {
            lblRowCol.setText(table.getWinner().getName() + " Win !!!");
        }
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setEnabled(false);
            }
        }
        showNewGame();
    }

    private void newGame() {
        table = new Table(o, x);
        showTable();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setEnabled(true);
            }
        }
    }

    private void showScore() {
        OWin.setText("OWin : " + o.getWin());
        OLose.setText("OLose : " + o.getLose());
        ODraw.setText("ODraw : " + o.getDraw());
        XWin.setText("XWin : " + x.getWin());
        XLose.setText("XLose : " + x.getLose());
        XDraw.setText("XDraw : " + x.getDraw());

    }
}
